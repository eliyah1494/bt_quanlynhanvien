function kiemTraRong(value, idErr) {
  if (value.length == 0) {
    document.getElementById(idErr).innerText = "Trường này không được để trống";
    document.getElementById(idErr).style.display = "block";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function kiemTraTaiKhoan(value, idErr) {
  const re = /^[0-9]{4,6}$/i;
  var isId = re.test(value);
  if (!isId) {
    document.getElementById(idErr).innerText = "Tài khoản không hợp lệ";
    document.getElementById(idErr).style.display = "block";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}
function kiemTraTen(value, idErr) {
  const re = /^[a-zA-Z]+$/;
  var isName = re.test(value);
  if (!isName) {
    document.getElementById(idErr).innerText = "Tên không hợp lệ";
    document.getElementById(idErr).style.display = "block";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function kiemTraEmail(value, idErr) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(value);
  if (!isEmail) {
    document.getElementById(idErr).innerText = "Email không hợp lệ";
    document.getElementById(idErr).style.display = "block";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function kiemTraMatKhau(value, idErr) {
  const re = /^(?=.*\d)(?=.*[A-Z])[a-zA-Z0-9]{6,10}$/;
  var isPassWord = re.test(value);
  if (!isPassWord) {
    document.getElementById(idErr).innerText = "Mật khẩu không hợp lệ";
    document.getElementById(idErr).style.display = "block";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function kiemTraNgayLam(value, idErr) {
  const re = /^\d{2}\/\d{2}\/\d{4}$/;
  var isDate = re.test(value);
  if (!isDate) {
    document.getElementById(idErr).innerText = "Ngày làm không hợp lệ";
    document.getElementById(idErr).style.display = "block";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function kiemTraLuongCB(value, idErr) {
  const re = /[1-9]\d{1,8}\d{3}/;
  var isSalary = re.test(value);
  if (!isSalary) {
    document.getElementById(idErr).innerText = "Lương không hợp lệ";
    document.getElementById(idErr).style.display = "block";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function kiemTraGioLam(value, idErr) {
  if (1 * value >= 80 && 1 * value <= 200) {
    document.getElementById(idErr).innerText = "";
    return true;
  } else {
    document.getElementById(idErr).innerText = "Giờ không hợp lệ";
    document.getElementById(idErr).style.display = "block";
    return false;
  }
}
