function NhanVien(
  _taiKhoan,
  _ten,
  _email,
  _matKhau,
  _ngayLam,
  _luongCoBan,
  _chucVu,
  _gioLam
) {
  this.taiKhoan = _taiKhoan;
  this.ten = _ten;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _ngayLam;
  this.luongCoBan = _luongCoBan;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;

  this.tongLuongNhanVien = function () {
    var selectValue = document.getElementById("chucvu");
    if (selectValue.options[1].text == this.chucVu) {
      var tongLuong = `${new Intl.NumberFormat("de-DE").format(
        this.luongCoBan * 3
      )} VND`;
    } else if (selectValue.options[2].text == this.chucVu) {
      var tongLuong = `${new Intl.NumberFormat("de-DE").format(
        this.luongCoBan * 2
      )} VND`;
    } else if (selectValue.options[3].text == this.chucVu) {
      var tongLuong = `${new Intl.NumberFormat("de-DE").format(
        this.luongCoBan * 1
      )} VND`;
    }
    return tongLuong;
  };

  this.xepLoaiNv = function () {
    var tongGioLam = this.gioLam * 1;
    if (tongGioLam >= 192) {
      var xepLoai = "Xuất sắc";
    } else if (tongGioLam >= 176) {
      var xepLoai = "Giỏi";
    } else if (tongGioLam >= 160) {
      var xepLoai = "Khá";
    } else {
      var xepLoai = "Trung bình";
    }
    return xepLoai;
  };
}
