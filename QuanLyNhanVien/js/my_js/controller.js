function layThongTinTuForm() {
  //   lấy thông tin từ user
  var taiKhoan = document.getElementById("tknv").value.trim();
  var ten = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var matKhau = document.getElementById("password").value.trim();
  var ngayLam = document.getElementById("datepicker").value.trim();
  var luongCoBan = document.getElementById("luongCB").value.trim();
  var chucVu = document.getElementById("chucvu").value.trim();
  var gioLam = document.getElementById("gioLam").value.trim();

  //   tạo object từ thông tin lấy từ form
  var nv = new NhanVien(
    taiKhoan,
    ten,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
  return nv;
}

function renderDsnv(list) {
  //   render danh sách
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var currentNv = list[i];
    var contentTr = `<tr>
    <td>${currentNv.taiKhoan}</td>
    <td>${currentNv.ten}</td>
    <td>${currentNv.email}</td>
    <td>${currentNv.ngayLam}</td>
    <td>${currentNv.chucVu}</td>
    <td>${currentNv.tongLuongNhanVien()}</td>
    <td>${currentNv.xepLoaiNv()}</td>
    <td><button class="btn btn-danger" onclick ="xoaNv('${
      currentNv.taiKhoan
    }')" class="btn btn-primary">Xoá</button>
    <button 
    data-toggle="modal"
    data-target="#myModal" onclick="suaNv('${
      currentNv.taiKhoan
    }')" class=" btn btn-primary">Sửa</button></td>
    </tr>`;

    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCoBan;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}

// reset form
