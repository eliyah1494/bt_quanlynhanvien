var dsnv = [];
const DSNV = "DSNV";
var dataJson = localStorage.getItem(DSNV);
if (dataJson) {
  dataRaw = JSON.parse(dataJson);
  dsnv = dataRaw.map(function (item) {
    return new NhanVien(
      item.taiKhoan,
      item.ten,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCoBan,
      item.chucVu,
      item.gioLam
    );
  });
  renderDsnv(dsnv);
}
function saveLocalStorage() {
  var dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, dsnvJson);
}

function themNv() {
  var newNv = layThongTinTuForm();

  var isValid = true;

  // kiểm tra tài khoản nv
  isValid = isValid &=
    kiemTraRong(newNv.taiKhoan, "tbTKNV") &&
    kiemTraTaiKhoan(newNv.taiKhoan, "tbTKNV");
  // kiểm tra tên
  isValid = isValid &=
    kiemTraRong(newNv.ten, "tbTen") && kiemTraTen(newNv.ten, "tbTen");
  // kiểm tra email
  isValid = isValid &=
    kiemTraRong(newNv.email, "tbEmail") && kiemTraEmail(newNv.email, "tbEmail");
  // kiểm tra mật Khẩu
  isValid = isValid &=
    kiemTraRong(newNv.matKhau, "tbMatKhau") &&
    kiemTraMatKhau(newNv.matKhau, "tbMatKhau");
  // kiểm tra ngày làm
  isValid = isValid &=
    kiemTraRong(newNv.ngayLam, "tbNgay") &&
    kiemTraNgayLam(newNv.ngayLam, "tbNgay");
  //  kiểm tra lương cơ bản
  isValid = isValid &=
    kiemTraRong(newNv.luongCoBan, "tbLuongCB") &&
    kiemTraLuongCB(newNv.luongCoBan, "tbLuongCB");
  // kiểm tra chức vụ
  isValid = isValid &= kiemTraRong(newNv.chucVu, "tbChucVu");
  // kiểm tra giờ làm
  isValid = isValid &=
    kiemTraRong(newNv.gioLam, "tbGiolam") &&
    kiemTraGioLam(newNv.gioLam, "tbGiolam");

  if (isValid) {
    dsnv.push(newNv);
    saveLocalStorage();
    renderDsnv(dsnv);
  }
}

function xoaNv(idNv) {
  // tìm vị trí đối tượng
  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == idNv;
  });
  if (index == -1) {
    return;
  }
  dsnv.splice(index, 1);
  renderDsnv(dsnv);
  saveLocalStorage();
}
function suaNv(idNv) {
  console.log(idNv);
  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == idNv;
  });
  console.log("index: ", index);
  if (index == -1) return;
  var nv = dsnv[index];
  console.log(nv);
  showThongTinLenForm(nv);
  document.getElementById("tknv").disabled = true;
}

function capNhatNv() {
  var nvEdit = layThongTinTuForm();
  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == nvEdit.taiKhoan;
  });

  if (index == -1) return;

  var isValid = true;

  // kiểm tra tài khoản nv
  isValid = isValid &= kiemTraRong(nvEdit.taiKhoan, "tbTKNV");
  // kiểm tra tên
  isValid = isValid &=
    kiemTraRong(nvEdit.ten, "tbTen") && kiemTraTen(nvEdit.ten, "tbTen");
  // kiểm tra email
  isValid = isValid &=
    kiemTraRong(nvEdit.email, "tbEmail") &&
    kiemTraEmail(nvEdit.email, "tbEmail");
  // kiểm tra mật Khẩu
  isValid = isValid &=
    kiemTraRong(nvEdit.matKhau, "tbMatKhau") &&
    kiemTraMatKhau(nvEdit.matKhau, "tbMatKhau");
  // kiểm tra ngày làm
  isValid = isValid &=
    kiemTraRong(nvEdit.ngayLam, "tbNgay") &&
    kiemTraNgayLam(nvEdit.ngayLam, "tbNgay");
  //  kiểm tra lương cơ bản
  isValid = isValid &=
    kiemTraRong(nvEdit.luongCoBan, "tbLuongCB") &&
    kiemTraLuongCB(nvEdit.luongCoBan, "tbLuongCB");
  // kiểm tra chức vụ
  isValid = isValid &= kiemTraRong(nvEdit.chucVu, "tbChucVu");
  // kiểm tra giờ làm
  isValid = isValid &=
    kiemTraRong(nvEdit.gioLam, "tbGiolam") &&
    kiemTraGioLam(nvEdit.gioLam, "tbGiolam");

  if (isValid) {
    dsnv[index] = nvEdit;
    saveLocalStorage();
    renderDsnv(dsnv);
  }

  document.getElementById("tknv").disabled = false;
}
function timNhanVien() {
  var loaiValue = document.getElementById("searchName").value;
  var timKiemLoai = dsnv.filter((value) => {
    return value.xepLoaiNv().toUpperCase().includes(loaiValue.toUpperCase());
  });
  renderDsnv(timKiemLoai);
}
